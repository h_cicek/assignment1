$(document).ready(function(){
  $("[data-role=header]").toolbar();
  $("[data-role=header]").navbar();

  $(document).on("pagecontainerchange", function(){
    let title = $(".ui-page-active").attr("data-title"); //to highlight the active page

    $("[data-role=header] h1").text(title);
  });
  $(document).change(function() { 
    $(".onOff").toggleClass("dark-mode"); //function for dark mode
});
});

